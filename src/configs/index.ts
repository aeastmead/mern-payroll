import DefaultConfig from "./default";

export interface IConfig{
  dbURI:string;
  port:string;
  secret:string;
  queryLimit:number;
  jwt:{jwsUri:string, audience:string, issuer:string};
}
const Config=<IConfig>Object.assign({},DefaultConfig,require('./'+process.env.NODE_ENV||'local'));

export default Config;
