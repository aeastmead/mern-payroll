export default {
  dbURI:process.env.DB_URI||"mongodb://localhost/payroll_local",
  rootUrl:process.env.ROOT_URL||"http://localhost:3000",
  jwt: {
    jwksUri: 'https://aeastmead.auth0.com/.well-known/jwks.json',
    audience: 'http://payroll.local',
    issuer: "https://aeastmead.auth0.com/"
  },
  port:process.env.PORT||3000,
  facebookAppId:process.env.FB_ID_RP,
  facebookAppSecret:process.env.FB_SECRET_RP,
  queryLimit:25,
  secret:'NW3vW1wazteBqpYSxHWjso6E0HifIsJm3L4znaF2ysJKt0rccmCg87m6zXLU9eNN'
}