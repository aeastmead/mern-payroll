/// <reference path="express-winston.d.ts" />
declare module "json!*" {
    const value: any;
    export default value;
}
declare module '*.scss' {
    const content: any;
    export default content;
}
declare module '*.css' {
    const content: any;
    export = content;
}
declare module "*.json" {
    const value: Object;
    export default value;
}
import * as Immutable from 'immutable';

declare module "immutable"{
    export module Record {
        export interface Class {
            new (): Immutable.Map<string, any>;
            new (values: {[key: string]: any}): Immutable.Map<string, any>;
            new (values: Iterable<string, any>): Immutable.Map<string, any>; // deprecated

            (): Immutable.Map<string, any>;
            (values: {[key: string]: any}): Immutable.Map<string, any>;
            (values: Iterable<string, any>): Immutable.Map<string, any>; // deprecated
        }
    }

    export function Record(
        defaultValues: {[key: string]: any}, name?: string
    ): Record.Class;
}

declare module "mongoose"{
    export interface Document{
        id:string;
        createdAt:Date;
        updatedAt:Date;
    }
    export type IDProperty=string | Types.ObjectId;
    export type ReferenceProperty<TDoc>=string |TDoc;
    export type IDocument<TDoc>=Document & TDoc;
}
