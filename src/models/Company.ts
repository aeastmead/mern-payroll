import * as mongoose from 'mongoose';
import {NameDef, ContactDef,ActiveDatesDef, AddressDef,Contact ,ActiveDates, Name, Address} from "./SubDocuments";


let CompanySchema = new mongoose.Schema({
        name:{type:String, required:true},
        contact:ContactDef,
        owner:NameDef,
address:AddressDef,
    activeDates:ActiveDatesDef,
        isDefault:{type:Boolean, required:false, default:false}
    });

export interface ICompany {
    name:string;
    contact:Contact;
    address:Address;
    owner:Name;
    activeDates?:ActiveDates;
}
export interface Company extends ICompany{
}
export interface Company extends mongoose.RxDocument, ICompany {
}

export interface CompanyModel extends mongoose.RxModel<Company> {

}

export const CompanyModel = mongoose.model<Company, CompanyModel>('Company', CompanySchema);
