import * as mongoose from 'mongoose';

export interface Contact{
    email?:string;
    phone?:string;
}

export const ContactDef:mongoose.SchemaDefinition={
    email:{type:mongoose.SchemaTypes.String, required:false},
    phone:{type:mongoose.SchemaTypes.String, required:false}
};