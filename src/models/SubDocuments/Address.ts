import * as mongoose from 'mongoose';
import {StateAbbrev} from "../../lib"

export interface Address{
    street1:string;
    street2?:string;
    city:string;
    state:string;
    zip:string;
}
export const AddressDef:mongoose.SchemaDefinition={
    street1:{type: String, required: true},
    street2:{type:String, required:false},
    city: {type: String, required: true},
    state: {type: String, required: true, enum:StateAbbrev},
    zip:{type: String, required: true, minlength:5}
};