import * as mongoose from 'mongoose';

export interface ActiveDates{
    start?:Date;
    end?:Date;
}
export const ActiveDatesDef={
 
    start:{type:mongoose.SchemaTypes.Date, required:false},
    end:{type:mongoose.SchemaTypes.Date, required:false}
};
