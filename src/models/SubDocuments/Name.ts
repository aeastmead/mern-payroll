import * as mongoose from 'mongoose';

export interface Name{
    firstName:string;
    lastName:string;
}

export const NameDef:mongoose.SchemaDefinition={
    firstName:{type: String, required: true},
    lastName:{type:String, required:true}
};
