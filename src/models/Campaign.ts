import * as mongoose from 'mongoose';
import {ActiveDatesDef,ActiveDates} from "./SubDocuments"

export interface ICampaign{
    name:string;
    activeDates?:ActiveDates;

}
export interface Campaign extends ICampaign{
}
export interface Campaign extends mongoose.RxDocument, ICampaign{
}

const CampaignSchema=new mongoose.Schema({
    name:{type:String, required:true},
    activeDates:ActiveDatesDef
});


export interface CampaignModel extends mongoose.RxModel<Campaign>{

}

export const CampaignModel=mongoose.model<Campaign,CampaignModel>('Campaign',CampaignSchema);
