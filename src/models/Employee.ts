import * as mongoose from 'mongoose';
import {NameDef, ContactDef,ActiveDatesDef, AddressDef,Contact ,ActiveDates, Name, Address} from "./SubDocuments";
import {Company} from "./Company"


const EmployeeSchema = new mongoose.Schema({
        company:{type:mongoose.SchemaTypes.ObjectId, ref:'Company', required:true},
        contact:ContactDef,
        address:AddressDef,
       name:NameDef,
    activeDates:ActiveDatesDef,
    });

export interface IEmployee {
    company:mongoose.ReferenceProperty<Company>;
    name:Name;
    address:Address;
    contact:Contact;
    activeDates?:ActiveDates;
}
export interface Employee extends IEmployee{

}
export interface Employee extends mongoose.RxDocument, IEmployee {

}



export interface EmployeeModel extends mongoose.RxModel<Employee> {

}

export const EmployeeModel = mongoose.model<Employee, EmployeeModel>('Employee', EmployeeSchema);
