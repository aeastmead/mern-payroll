
import * as passport from "passport";

import {Strategy as JWTStrategy,ExtractJwt} from 'passport-jwt';

export function SetupJwt(){

    passport.use(new JWTStrategy({
        jwtFromRequest:ExtractJwt.fromAuthHeader(),
        secretOrKey:"2s132O5aHcu2UyT2p648azRndDO3mY0U"
    }, function(jwt_payload, done) {
        done(null,{id:jwt_payload.sub});
    }));
}
export const Authenticate=()=>passport.authenticate('jwt',{session:false});