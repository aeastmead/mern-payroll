import * as mongoose from 'mongoose';
import {bindNodeCallback as bindNodeCallback$} from "rxjs/Observable/bindNodeCallback"
import {Observable} from "rxjs/Observable";
import "rxjs/add/operator/map";

import {RxRepositoryPlugin} from "../dbPlugins";
import Config from "../../configs";

export function Connect$():Observable<mongoose.Connection>{
    (mongoose as any).Promise=require('bluebird');
    (mongoose as any).plugin(RxRepositoryPlugin);
    return bindNodeCallback$<string,void>(mongoose.connect)(Config.dbURI)
        .map(()=>mongoose.connection);
}
