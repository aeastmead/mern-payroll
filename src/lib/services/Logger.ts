import * as winston from "winston";
import {logger, errorLogger} from "express-winston";

export function LogMiddleware(){
    return logger({
      transports: [
        new winston.transports.Console({
          json: true,
          colorize: true
        })
      ]
    });
}

export function ErrorLogMiddleware(){
    return errorLogger({
      transports: [
        new winston.transports.Console({
          json: true,
          colorize: true
        })
      ]
    });
}