
import * as _ from "lodash";
export function isEmpty(val:any){
    if(_.isUndefined(val))
    return true;
    if(_.isNil(val))
    return true;
    if(_.isObject(val)){
        return Object.keys(val).length>0;
    }
    if(_.isString(val))
        return val.trim().length<=0;
    return _.isEmpty(val);
}