import * as mongoose from "mongoose";
import * as mongoosePaginate from 'mongoose-paginate';
import * as _ from "lodash";
import * as plural from "pluralize";

import {bindNodeCallback as bindNodeCallback$} from "rxjs/Observable/bindNodeCallback"
import {Observable} from "rxjs/Observable";
import {Observer} from "rxjs/Observer";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import {NotFound} from 'http-errors';

declare module 'mongoose'{
    interface RxPaginationResult<T extends mongoose.Document>{
        total:number;
        limit?:number;
        offset:number;
        include?:string[];
        sort_direction:string;
        sort_field:string;
        data:T[];
    }
    interface RxDocument extends mongoose.Document{
        update$(newData:Object):Observable<this>;
    }
    interface RxModel<T extends mongoose.RxDocument> extends PaginateModel<T>{
        insert$(doc:Object):Observable<T>;
        findById$(id:string,include?:string[]|string):Observable<T>;
        find$(query:Object,options:mongoose.PaginateOptions):Observable<mongoose.RxPaginationResult<T>>;
        all$(sort?:Object,populate?:string[]):Observable<mongoose.RxPaginationResult<T>>;
        paginate$(query:Object,options:mongoose.PaginateOptions):Observable<mongoose.RxPaginationResult<T>>;
        getKind():string;
    }
}

    interface RxMapToPaginationResponse<T extends mongoose.RxDocument>{
        (options:mongoose.PaginateOptions):(result:mongoose.PaginateResult<T>)=>mongoose.RxPaginationResult<T>
    }
export default function RxRepositoryPlugin<T extends mongoose.RxDocument>(schema:mongoose.Schema,options:any){
    schema.set('toObject',{getters:true,versionKey:false});
    schema.plugin(mongoosePaginate);
    schema.set('timestamps',true);
    schema.set('id',true);
    let mapToPaginationResponse:RxMapToPaginationResponse<T>=({sort,populate}:mongoose.PaginateOptions)=>{
        return ({total,limit,offset=0,docs:data}:mongoose.PaginateResult<T>)=>{
            if( total<=0){
                throw new NotFound()
            }
            let sort_field:string=Object.keys(sort)[0];
            return {include:populate as string[], total,limit,offset,data, sort_field, sort_direction:sort[sort_field]};
        }        
    }

    schema.static('getKind',function getKind(this:mongoose.RxModel<T>){
        return _.lowerCase(plural(this.modelName));
    });
    schema.static('paginate$',function paginate$(this:mongoose.RxModel<T>,query:Object,options:mongoose.PaginateOptions):Observable<mongoose.RxPaginationResult<T>>{
        return bindNodeCallback$<Object,mongoose.PaginateOptions,mongoose.PaginateResult<T>>(this.paginate)(query,options)
                .map(mapToPaginationResponse(options));        
    })
    schema.static('findById$',function findById$(this:mongoose.RxModel<T>,id:string,include:string[]=[]){
        let mquery:mongoose.DocumentQuery<T,T>=this.findById(id);
        include.forEach(path=>mquery=mquery.populate(path))
        return bindNodeCallback$<T>(mquery.exec)();
    });
    schema.static('find$',function find$(this:mongoose.RxModel<T>,query:Object,options:mongoose.PaginateOptions):Observable<mongoose.RxPaginationResult<T>>{
        return this.paginate$(query,options);
    });
    schema.static('all$',function all$(this:mongoose.RxModel<T>,sort:Object={createdAt:'desc'},populate?:string[]):Observable<mongoose.RxPaginationResult<T>>{
        let options:mongoose.PaginateOptions={sort,populate};
        return this.paginate$({},{sort,populate});
    });
    schema.static('insert$',function all$(this:mongoose.RxModel<T>,docObj:Object){
        let doc:T=new this(docObj);
        return bindNodeCallback$<T>(doc.save)();
    });
    schema.method('update$',function(this:T,newData:Object):Observable<T>{
        this.set(newData);
        return bindNodeCallback$<T>(this.save)()
    })
}