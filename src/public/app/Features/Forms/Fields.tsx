import * as React from 'react'
import {WrappedFieldProps} from 'redux-form'
import {Form, FormFieldProps,FormInputProps, FormSelectProps} from 'semantic-ui-react'

type ReduxFormFieldComponentProps=WrappedFieldProps<any>;
export interface ReduxFormFieldMapToProps<P extends FormFieldProps>{
    (props:ReduxFormFieldComponentProps & P ):P & React.Attributes
}
function mapFieldPropsToProps({input,meta,...formFieldProps}:ReduxFormFieldComponentProps & FormFieldProps){
    console.log(formFieldProps)
    return{
        ...formFieldProps,
        ...input
    }
}
function createFormFieldComponent<P extends FormFieldProps>(UIComponent: React.StatelessComponent<P>, mapFieldToProps:ReduxFormFieldMapToProps<P>):React.SFC<P> {
  return function(props:ReduxFormFieldComponentProps&P):React.SFCElement<P>{
      console.log('props')
      console.log(props)
      return React.createElement<P>(UIComponent,Object.assign({},mapFieldToProps(props),{error:props.meta.touched && props.meta.error ? true: null }))
  }
}

export const Input:React.SFC<FormInputProps>=createFormFieldComponent<FormInputProps>(Form.Input,mapFieldPropsToProps);

//export const Select:React.SFC<FormSelectProps>=createFormFieldComponent<FormSelectProps>(Form.Select,mapFieldPropsToProps);
