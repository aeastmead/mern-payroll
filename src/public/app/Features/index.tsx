import * as React from 'react';
import {Store} from 'redux';
import { Provider } from 'react-redux';
import {Route,Switch} from 'react-router';
import { ConnectedRouter} from 'react-router-redux';
import {History} from 'history';
import {RootState} from "../modules";
import Login from "./Login/index";
import Home from "./Home/index";

interface ComponentProps {
    store:Store<RootState>;
    history:History;
}
interface ComponentStates {
}

const styles={
    wrapper:{display:'flex' as any,flexDirection:'column' as any, justifyContent:'flex-start' as any,height:"100%", width:"100%"}
}
export default class App extends React.Component<ComponentProps, ComponentStates> {


    render() {
        const {store,history} = this.props;
        return (
            <Provider store={store}>
                <ConnectedRouter history={history}>
                        <Switch>
                            <Route exact strict path="/login" component={Login}/>
                                <Route exact strict path="/" component={Home}/>
                        </Switch>
                </ConnectedRouter>
            </Provider>
        )
    }
}