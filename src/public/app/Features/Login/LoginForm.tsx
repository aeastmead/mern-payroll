import * as React from 'react';
import{ Field} from 'redux-form';
import {Button,  Message, Grid, Form} from 'semantic-ui-react';
import {Input} from '../Forms'
interface ComponentProps{
    handleSubmit:React.FormEventHandler<HTMLFormElement>;
    hasError?:boolean;
    submitting?:boolean;

}

export default function LoginForm({hasError,handleSubmit,submitting}:ComponentProps){
    return(
        <Form onSubmit={handleSubmit} loading={submitting ? true:null}>
            <Field name="username" type="email" placeholder="name@gmail.com" component={Input} />
            <Field name="password" type="password" placeholder="****" component={Input} />
            <Button type="submit">Login</Button>
        </Form>
    )
}
