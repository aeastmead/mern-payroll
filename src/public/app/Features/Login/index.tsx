import * as React from 'react';
import {connect, Dispatch} from 'react-redux';
import {RouteComponentProps} from "react-router";
import {replace} from "react-router-redux";
import {reduxForm,Field} from "redux-form/immutable";
import {FormProps} from "redux-form";

import {Button, Form, Message, Grid} from 'semantic-ui-react'
import {login,LoginModel, isAuthSelector,AuthActionCreators} from "../../modules/auth";
import LoginForm from "./LoginForm";
import {RootState} from "../../modules/index";
import {bindActionCreators} from "redux";

interface StateProps{
    isAuth:boolean;
}
interface DispatchProps{
    replace:typeof replace;
}
interface ConnectedComponentProps extends RouteComponentProps<any>,StateProps,DispatchProps{
}
interface ComponentProps extends ConnectedComponentProps,FormProps<LoginModel,ConnectedComponentProps,any>, React.HTMLProps<any>{
}
interface ComponentStates {
}
function mapStateToProps(state:RootState,ownProps:ConnectedComponentProps):StateProps{
    return {
        isAuth:isAuthSelector(state)
    }
}
function mapDispatchToProps(dispatch:Dispatch<RootState>):DispatchProps{
    
    return {   ...bindActionCreators({replace},dispatch),
               onSubmit:(formData:LoginModel)=>login(formData),
       onSubmitSuccess:()=>{
           dispatch(AuthActionCreators.loggedIn())
           return dispatch(replace('/'))
       }
    }
}
const styles={
    wrapper:{
        width:'100%',
        height:'100%',
        background:'#666'
    },
    container:{
        width:360,
        margin:'7% auto',
        padding:20,
        background:"#FFFFFF"
    },
    headerRow:{
        marginBottom:30,
        textAlign:'center'
    },
fieldRow:{
        width:'100%',
    marginBottom:20
},
    buttonRow:{
        width:'100%',
        textAlign:'center' as any
    }
}
class LoginComponent extends React.Component<ComponentProps, ComponentStates> {
    componentWillMount(){
        if(this.props.isAuth){
            this.props.replace('/');
        }
    }

    render() {
        const {handleSubmit, submitting} = this.props;
        return (
            <Grid verticalAlign="middle" centered columns={1} textAlign="center">
                <Grid.Column tablet={10} mobile={16} computer={6}>
                    <LoginForm handleSubmit={handleSubmit} submitting={submitting} />
                </Grid.Column>
            </Grid>
        )
    }
}
let FormComponent=reduxForm<LoginModel,ConnectedComponentProps,any>({
    form:'login'
})(LoginComponent);
const Login=connect<StateProps,DispatchProps,ConnectedComponentProps>(mapStateToProps,mapDispatchToProps)
(FormComponent);
export default Login;
