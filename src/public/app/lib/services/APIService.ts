import AjaxService from './AjaxService';
import Auth0Service from "./Auth0Service"
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/operator/mergeMap";
import "rxjs/add/operator/filter";
import "rxjs/add/observable/of";
import {Observable} from "rxjs/Observable";
import NormalizrService from "./NormalizrService";
import {INormalize,IAPI} from "../interfaces";
import {isEmpty} from "../utils"

namespace APIService{

    const basePath="/api/v1";
    const getAuthHeader=()=>({
        Authorization: `JWT ${Auth0Service.getAccessToken()}`
    });
    const buildUrl=(path:string,id?:string)=>{
        let url=`${basePath}/${path}`;
        if(id)
            url=`${url}/${id}`;
        return url;
    }
    export function getAll(path:string):Observable<IAPI.IPaginationResponse>{
        return AjaxService
            .getJSON<IAPI.IPaginationResponse>(buildUrl(`${path}/list`),undefined,getAuthHeader());
    }
    export function getPage(path:string,queryString?:IAPI.IRequestPageQuery):Observable<INormalize.INormalizedPagination>{
        return AjaxService
            .getJSON<IAPI.IPaginationResponse>(buildUrl(path),queryString,getAuthHeader())
            .map(NormalizrService.fromPageApi)
    }
    export function getItem(path:string,id:string,queryString?:IAPI.IRequestItemQuery):Observable<INormalize.INormalizedItem>{
        return AjaxService
            .getJSON<IAPI.IPaginationResponse>(buildUrl(path,id),queryString,getAuthHeader())
            .map(response=>NormalizrService.fromSingleApi(response))
    }
    export function post(path:string,body:Object):Observable<INormalize.INormalizedItem>{
        return AjaxService.postJSON<IAPI.ISingleResponse>(buildUrl(path),body,getAuthHeader())
            .map(response=>NormalizrService.fromSingleApi(response));
    }
    export function put(path:string,id:string,body:Object):Observable<INormalize.INormalizedItem>{
        return  AjaxService.putJSON<IAPI.ISingleResponse>(buildUrl(path,id),body,getAuthHeader())
            .map(response=>NormalizrService.fromSingleApi(response));
    }
    export function save(path:string,body:Object,id?:string){
        if(!isEmpty(id))
            return put(path,id,body);
        else
            return post(path,body);
    }
}

export default APIService;