
import {_throw as throw$} from "rxjs/Observable/throw";
import * as jwt_decode from "jwt-decode";
import AjaxService from "./AjaxService";
import ILoginModel from "../interfaces/LoginModel";

namespace Auth0Service{
    export interface IResponse{
        access_token?:string;
        token_type:string;
        id_token?:string;
        refresh_token?:string;
    }
    const domain="aeastmead.auth0.com";
    const clientId="IRr39qrxy72yKTTdO9aSNlMsYPS7SUSJ";
    const apiId="http://payroll.api.dev";
    const oauthEndpoint=`https://${domain}/oauth/token`;
    const device='web-app';
    function request(body:Object){
        body['client_id']=clientId;
        return AjaxService.postJSON<IResponse>(oauthEndpoint,body)
            .map(handleResponse);
    }
    function handleResponse(result:IResponse){
        ['access_token','refresh_token']
            .forEach(token=>{
                if(result[token]){
                    localStorage.setItem(token,result[token]);
                }
            })
        return {};
    }
    function isTokenExpired(token:string){
        if(!token)
            return true;
        return Math.floor(Date.now()/ 1000)>(jwt_decode (token).exp-(20*60 *60));
    }
    function getRefreshToken(){
        return localStorage.getItem('refresh_token');
    }
   export function getAccessToken(){
        let token= localStorage.getItem('access_token');
        if(token && isTokenExpired(token)){
            localStorage.removeItem('access_token');
            token=undefined;
        }
        return token;

    }
    export function isLoggedIn(){
        return !!getAccessToken();
    }
    export function login(fields:ILoginModel){
        return request({
            audience:apiId,
            connection:'Username-Password-Authentication',
            grant_type:"password",
            scope:'offline_access',
            device:'web-app',
            ...fields
        });
    }
    export function refresh(){
        let token=localStorage.getItem('refresh_token');
        if(!token)
            return throw$(new Error('no token'));
        return request({
            grant_type: "refresh_token",
            refresh_token: getRefreshToken()})
    }
}
export default Auth0Service;