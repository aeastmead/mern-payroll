
import * as qs from 'qs';
import { ajax } from 'rxjs/observable/dom/ajax';
import {Observable} from "rxjs/Observable";
import {AjaxRequest, AjaxResponse} from "rxjs/Rx";
import {map} from "rxjs/operator/map";
import {isEmpty} from "../utils"

namespace AjaxService{
    export interface IRequest{
        body?:any;
        headers:Object;
        queryString?:Object;
    }
    function buildUrl(url:string,q?:Object){
        return url +(!isEmpty(q) ? '?'+qs.stringify(q):'');
    }
    function JSONMapper<T>(ajaxCall$:Observable<AjaxResponse>){
        return ajaxCall$.map(ajaxResponse=>ajaxResponse.response as T);
    }
    export function request(url:string, method:string, {queryString,headers,body}:IRequest){
        let ajaxReq:AjaxRequest={url:buildUrl(url,queryString),method:method,headers:headers};
        if(body)
            ajaxReq.body=body;
        return ajax(ajaxReq);
    }
    export function requestJSON<T>(url:string, method:string, requestObj:IRequest):Observable<T>{

        requestObj.headers['Content-Type']='application/json';
        return request(url,method,requestObj)
            .map(ajaxResponse=>ajaxResponse.response as T)
    }
    export function post(url:string,body:any,headers:Object={}){
        return ajax.post(url,'post',{body,headers});
    }

    export function postJSON<T>(url:string, body:Object={}, headers:Object={}) {
        return requestJSON<T>(url,'post',{body,headers});
    }

    export function put(url:string,body:Object,headers:Object={}){
        return request(url,'put',{body,headers});
    }

    export function putJSON<T>(url:string, body:Object={}, headers:Object={}) {
        return requestJSON<T>(url,'put',{body,headers});
    }
    export function get<TResponse>(url:string,queryString:Object={},headers:Object={}){
        return request(url,'get',{headers,queryString});
    }

    export function getJSON<T>(url:string,queryString:Object={},headers:Object={}){
        return requestJSON<T>(url,'get',{queryString,headers})
    }
}

export default AjaxService;