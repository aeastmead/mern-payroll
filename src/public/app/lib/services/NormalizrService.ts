import {schema,normalize as normaliz, denormalize as denormaliz} from "normalizr";
import {omit,merge,isArray} from 'lodash'

import {INormalize,IAPI} from "../interfaces";



namespace NormalizrService {
    let schemaArrays=new Map<string,schema.Array>();
    let schemas=new Map<string,schema.Entity>();
    schemas.set('campaigns',new schema.Entity('campaigns'));
    schemas.set('companies',new schema.Entity('companies'));
    schemas.set('employees',new schema.Entity('employees',{
        company:schemas.get('companies')
    }));
    schemas.forEach((schem,key)=>{
        schemaArrays.set(key,new schema.Array(schem))
    });
    export function normalize(entity: string, data: any):INormalize.INormalizedResult {
        return normaliz(data,isArray(data) ? schemaArrays.get(entity):schemas.get(entity));
    }
    export function denormalize<E>(entity: string, ids:string[]|string,entities:any):E {
        return denormaliz(ids,isArray(ids) ? schemaArrays.get(entity):schemas.get(entity),entities) as E;
    }
    export function fromPageApi(result:IAPI.IPaginationResponse):INormalize.INormalizedPagination{
        return merge(omit(result,['data']),normalize(result.kind,result.data)) as INormalize.INormalizedPagination
    }
    export function fromSingleApi(result:IAPI.ISingleResponse):INormalize.INormalizedItem{

        return merge(normalize(result.kind,result.data),{kind:result.kind}) as INormalize.INormalizedItem
    }
}

export default NormalizrService;