export  {default as AjaxService}from './AjaxService';
export {default as APIService} from   './APIService';
export {default as NormalizrService} from  './NormalizrService';
export {default as Auth0Service} from  './Auth0Service';