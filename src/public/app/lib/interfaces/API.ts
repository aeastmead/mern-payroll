
namespace IAPI {
    export interface IRequestPageQuery{
        limit?:number;
        offset?:number;
        include?:string[];
        sort?:string;

    }
    export interface IRequestItemQuery{
        include?:string[];
    }
    export interface ISingleResponse{
        kind:string;
        data:Object;
    }
    export interface IPagination{
        total:number;
        limit:number;
        offset:number;
        include:string[];
        sort:string;
        kind:string;

    }

    export interface IPaginationResponse extends IPagination{
        data:Object[];
    }
    export interface IErrorResponse{
        message:string;
        status:number;

    }
}

export default IAPI;