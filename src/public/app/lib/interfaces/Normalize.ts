
import IAPI from "./API";
namespace INormalize{
    export interface INormalizedResult{
        entities:{[entity:string]:any};

    }
    export interface INormalizedItem extends INormalizedResult{
        kind:string;
        result:string;
    }
    export interface INormalizedPagination extends IAPI.IPagination,INormalizedResult{
        result:string[];
    }
}
export default INormalize;