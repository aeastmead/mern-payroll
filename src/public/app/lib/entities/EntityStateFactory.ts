import * as Immutable from 'immutable';
import {Entity} from "./EntityFactory";

export type EntityState<T extends Entity>=Immutable.Map<string,T>;

export default function EntityStateFactory<T extends Entity>():EntityState<T>{
    return Immutable.Map<string,T>();
}