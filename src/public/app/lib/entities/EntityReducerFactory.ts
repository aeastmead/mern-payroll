import {Reducer} from "redux";
import * as _ from 'lodash'
import {EntityAction, Types} from "./EntityActionCreatorFactory";
import {Entity} from "./EntityFactory";
import EntityStateFactory,{EntityState} from "./EntityStateFactory";

/**
 * Entity Factory Reducer
 * @param {string} entity - entity name
 * @param {typeof Entity} newableEntity
 * @return {(state:EntityState<E>, {type,payload}:{type: any, payload: any})=>(EntityState<E>|EntityState<E>)}
 * @constructor
 */
export default function EntityReducerFactory<E extends Entity>(entity:string,newableEntity:{new(props?):E}):Reducer<EntityState<E>>{

 return (state:EntityState<E>=EntityStateFactory<E>(),{type,payload})=> {
  if(!payload||(payload && !payload.entities)||(payload && payload.entities && !payload.entities[entity]))
   return state;
   switch (type){
    case Types.FETCH_PAGE.SUCCESS:
    case Types.FETCH_ITEM.SUCCESS:
      let entities:{[id:string]:E}=payload.entities[entity];
      let immutables:{[id:string]:E}={};
      Object.keys(entities).forEach(id=>{
       immutables[id]=new newableEntity(entities[id]);
      });
      return state.merge(immutables);
    default:
     return state;
   }
 }
}