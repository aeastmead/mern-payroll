import { combineEpics } from 'redux-observable';
import {initialize} from "redux-form";
import * as _ from 'lodash'
import {of as of$} from "rxjs/Observable/of";
import {concat as concat$} from "rxjs/Observable/concat";

import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/operator/mergeMap";
import "rxjs/add/operator/merge";
import "rxjs/add/operator/delay";
import "rxjs/add/operator/startWith";
import APIService from "../services/APIService";
import {Types, IEntityActions, EntityActionCreator} from "./EntityActionCreatorFactory";

export namespace EntityEpics{
    export const entityActionFilter=(entity:string,filterType:string)=>
        (action=>!_.isUndefined(action.entity) && action.type==filterType && action.entity==entity);
    export const entityFetchPageEpic=(entity:string)=>
        (action$)=>
        action$.filter(entityActionFilter(entity,Types.FETCH_PAGE.REQUEST))
            .mergeMap((action:IEntityActions.IFetchPageRequestAction)=>APIService
                .getPage(action.entity,action.payload.queryString)
                .map(result=>EntityActionCreator.fetchPageSuccess(action.entity,result))
                    .catch(err=>of$(EntityActionCreator.fetchPageFailure(action.entity,err)))
            );
    export const entityFetchItemEpic=(entity:string)=>
        (action$)=>
            action$.filter(entityActionFilter(entity,Types.FETCH_ITEM.REQUEST))
                .mergeMap((action:IEntityActions.IFetchItemRequestAction)=>APIService
                    .getItem(action.entity,action.payload.id,{include:action.payload.include})
                    .mergeMap(result=>{
                        let successAction=of$(EntityActionCreator.fetchItemSuccess(action.entity,result));
                        if(action.payload.formName)
                            return concat$(successAction,of$(initialize(action.payload.formName,result.entities[action.entity][result.result])))
                        return successAction;
                    })
                    .catch(err=>of$(EntityActionCreator.fetchItemFailure(action.entity,err)))
                );
}

export default function EntityEpicsFactory(entity:string){
    return combineEpics(EntityEpics.entityFetchPageEpic(entity),EntityEpics.entityFetchItemEpic(entity));
}