
import APIService from "../services/APIService";
//import "rxjs/add/operator/toPromise";
export namespace EntityActions{
    export function submit(entity:string, data:Object, id?:string){
        return APIService.save(entity,data, id)
        .toPromise()
    }
}
export default function  EntityActionsFactory(entity:string){
    return{
        submit:(data:Object, id?:string)=>EntityActions.submit(entity,data,id)
    }
}