import * as Immutable from 'immutable';

export interface IEntity{
        id?:string;
        _id?:string;
    createdAt?:Date;
    updatedAt?:Date;
}
export class Entity extends Immutable.Record({id:null,createdAt:null,updatedAt:null}){
    id?:string;
    createdAt?:Date;
    updatedAt?:Date;
}

export default function EntityFactory<T>(defaultValues:T):typeof Entity{
    class Base extends Immutable.Record(Object.assign({},{id:null,createdAt:null,updatedAt:null},defaultValues)){
        id:string;
        createdAt:Date;
        updatedAt:Date;
    }
    return Base;
}