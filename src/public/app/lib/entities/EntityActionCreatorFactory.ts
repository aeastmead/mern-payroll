
import { IAPI, INormalize} from "../interfaces";

export interface EntityAction{
    type:string;
    entity:string;
    payload:Object;
}
export const Types={
    FETCH_PAGE:{
        SUCCESS:"entities/FETCH_PAGE_SUCCESS",
        FAILURE:'entities/FETCH_PAGE_FAILURE',
        REQUEST:'entities/FETCH_PAGE_REQUEST'
    },
    FETCH_ITEM:{
        SUCCESS:"entities/FETCH_ITEM_SUCCESS",
        FAILURE:'entities/FETCH_ITEM_FAILURE',
        REQUEST:'entities/FETCH_ITEM_REQUEST',
        CLEAR:'entities/FETCH_ITEM_CLEAR'
        
    },
    SUBMIT:{
        SUCCESS:"entities/SUBMIT_SUCCESS",
        FAILURE:'entities/SUBMIT_FAILURE',
        REQUEST:'entities/SUBMIT_REQUEST'

    },
    ITEM_RESET:'entities/ITEM_RESET'
};
export namespace IEntityActions{
    export interface IEntityAction<T>{
        type:string,
        entity:string;
        payload:T;
    }
    export interface IFetchPageRequestPayload{
        queryString?:IAPI.IRequestPageQuery;
    }
    export interface IFetchPageRequestAction extends IEntityAction<IFetchPageRequestPayload>{
    }
    export interface IFetchItemRequestPayload{
        id:string,
        include?:string[],
        formName?:string
    }
    export interface IFetchItemRequestAction extends IEntityAction<IFetchItemRequestPayload>{

    }
    export interface ISubmitRequestPayload{
        body:Object;
        id?:string;
        isEdit:boolean;
    }
    export interface ISubmitRequestAction extends IEntityAction<ISubmitRequestPayload>{

    }

}
export const EntityActionCreator={
    fetchPageRequest:(entity:string,queryString?:IAPI.IRequestPageQuery)=>({
        type:Types.FETCH_PAGE.REQUEST,
        entity:entity,
        payload:{
            queryString
        }
    }),
    fetchPageSuccess:(entity:string,normalResponse:INormalize.INormalizedPagination)=>({
        type:Types.FETCH_PAGE.SUCCESS,
        entity:entity,
        payload:normalResponse
    }),
    fetchPageFailure:(entity:string,error:IAPI.IErrorResponse)=>({
        type:Types.FETCH_PAGE.FAILURE,
        entity:entity,
        payload:error
    }),
    fetchItemRequest:(entity:string,id:string,include?:string[],formName?:string)=>({
        type:Types.FETCH_ITEM.REQUEST,
        entity:entity,
        payload:{
            id,
            include,
            formName
        }
    }),
    fetchItemSuccess:(entity:string,normalResponse:INormalize.INormalizedItem)=>({
        type:Types.FETCH_ITEM.SUCCESS,
        entity:entity,
        payload:normalResponse
    }),
    fetchItemFailure:(entity:string,error:IAPI.IErrorResponse)=>({
        type:Types.FETCH_ITEM.FAILURE,
        entity:entity,
        payload:error
    }),
    submitRequest:(entity:string,body:any,id?:string)=>({
        type:Types.SUBMIT.REQUEST,
        entity:entity,
        payload:{
            body,
            id,
            isEdit:!!id
        }
    }),
    submitSuccess:(entity:string,result:INormalize.INormalizedItem)=>({
        type:Types.SUBMIT.SUCCESS,
        entity:entity,
        payload:result
    }),

    submitFailure:(entity:string,error:IAPI.IErrorResponse)=>({
        type:Types.SUBMIT.FAILURE,
        entity:entity,
        payload:error
    }),
    itemReset:(entity:string)=>({
        type:Types.ITEM_RESET,
        entity:entity,
        payload:{}

    })
}
export default function EntityActionCreatorFactory(entity:string) {
    return {
        fetchPageRequest:(queryString?:IAPI.IRequestPageQuery)=>EntityActionCreator.fetchPageRequest(entity,queryString),
        fetchPageSuccess:(normalResponse:INormalize.INormalizedPagination)=>EntityActionCreator.fetchPageSuccess(entity,normalResponse),
        fetchPageFailure:(error:IAPI.IErrorResponse)=>EntityActionCreator.fetchPageFailure(entity,error),
        fetchItemRequest:(id:string,include?:string[],formName?:string)=>EntityActionCreator.fetchItemRequest(entity,id,include,formName),
        fetchItemSuccess:(normalResponse:INormalize.INormalizedItem)=>EntityActionCreator.fetchItemSuccess(entity,normalResponse),
        fetchItemFailure:(error:IAPI.IErrorResponse)=>EntityActionCreator.fetchItemFailure(entity,error),
        submitRequest:(body:any,id?:string)=>EntityActionCreator.submitRequest(entity,body,id),
        submitSuccess:(result:INormalize.INormalizedItem)=>EntityActionCreator.submitSuccess(entity,result),
        submitFailure:(error:IAPI.IErrorResponse)=>EntityActionCreator.fetchItemFailure(entity,error),
        itemReset:()=>EntityActionCreator.itemReset(entity)
    }
}