import * as _ from 'lodash'
import * as numeral from 'numeral'
import * as moment from "moment";
import {isEmpty} from "../utils"

export const moneyFormat=value => {
    if (!_.isNil(value) && !_.isEmpty(value)) {
        let num = numeral(value);
        if (num.isNumeral) {
            return num.formatCurrency();
        }
    }
    return '';
};
export const dateFieldFormat=value=>{
    if(isEmpty(value))
        return null;
    if(_.isString(value))
        return new Date(value);
    return value;
}
export const dateToStringFormat=(formatStr:string='MM/DD/YYYY')=>(value:Object)=>{
  return  isEmpty(value) ? '':moment(value).format(formatStr)
};