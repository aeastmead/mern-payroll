import * as validator from 'validator';
import * as moment from "moment";
import {isEmpty} from "../utils"

export const required= (value:any)=> isEmpty(value) ? 'Required':undefined;
export const dateValue=(value:any)=> {
    if(isEmpty(value))
    return undefined;
    let msg="Invalid date";
    return moment(value).isValid() ? undefined: "Invalid date";
}
export const minLength = (min:number) => (value:string) =>
  !isEmpty(value) && value.length<min ? `Must be at least ${min} charectors long` : undefined;

  export const emailValue=(value:any)=>!isEmpty(value) && !validator.isEmail(value) ? 'Invalid Email':undefined;