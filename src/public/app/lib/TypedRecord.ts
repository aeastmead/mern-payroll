import * as Immutable from 'immutable';

export interface IRecord<T>{
    new(values?:T):Immutable.Map<string,T>;
    (values?:T):Immutable.Map<string,T>;
}