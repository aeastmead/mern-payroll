import {EntityFactory} from "../entities";

interface IAttributes{
   firstName:string;
   lastName:string;
}
export default class Name extends EntityFactory<IAttributes>({firstName:'', lastName:''}){
   firstName:string;
   lastName:string;
  
}