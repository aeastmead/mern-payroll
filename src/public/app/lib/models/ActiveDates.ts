import {EntityFactory} from "../entities";

interface IAttributes{
   start?:Date;
    end?:Date;
}
export default class ActiveDates extends EntityFactory<IAttributes>({start:null,end:null}){
   start:Date;
    end:Date;
  
}