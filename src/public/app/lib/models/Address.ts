import {EntityFactory} from "../entities";

interface IAttributes{
    street1:string;
    street2?:string;
    city:string;
    state:string;
    zip:string;
}
export default class Address extends EntityFactory<IAttributes>({street1:null, street2:null,city:null, state:null, zip:null}){
    street1:string;
    street2?:string;
    city:string;
    state:string;
    zip:string;

}