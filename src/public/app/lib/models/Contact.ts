import {EntityFactory} from "../../lib";
import Address from "./Address"

interface IAttributes{
   email?:string;
   phone?:string;
   address:Address;
}
export default class Contact extends EntityFactory<IAttributes>({email:null, phone:null, address:new Address()}){
   email?:string;
   phone?:string;
   address:Address;
  

}