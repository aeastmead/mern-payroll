import { EntityReducerFactory} from "../../lib";
import {Company} from "./models";

import EntityNames from "../entityNames";

export default EntityReducerFactory<Company>(EntityNames.COMPANIES,Company);
