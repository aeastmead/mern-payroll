import {EntityFactory,EntityStateFactory,EntityState,Contact, Name, ActiveDates, IEntity} from "../../lib";

interface IAttributes{
    name:string;
    owner:Name;
    contact:Contact;
    activeDates:ActiveDates;

}
export interface ICompany extends IEntity{
    name:string;
    owner:Name;
    contact:Contact;
    activeDates:ActiveDates;

}
export class Company extends EntityFactory<IAttributes>({name:null, owner:new Name(), contact:new Contact(), activeDates:new ActiveDates()}){
    name:string;
    owner:Name;
    contact:Contact;
    activeDates:ActiveDates;

}
export type CompaniesState=EntityState<Company>;
export const CompaniesState:CompaniesState = EntityStateFactory<Company>();