import {selectEntityPageFactory,selectEntityPageLoadingFactory} from "../paging";
import {Company } from "./models";
import EntityNames from "../entityNames";
import {selectEntityItemFactory, selectEntityItemLoadingFactory, selectEntityItemSubmittingFactory} from "../item";


export const selectCompaniesPage=selectEntityPageFactory<Company>(EntityNames.COMPANIES);
export const selectCompaniesPageLoading=selectEntityPageLoadingFactory(EntityNames.COMPANIES);
export const selectCompaniesItem=selectEntityItemFactory<Company>(EntityNames.COMPANIES);
export const selectCompaniesItemLoading=selectEntityItemLoadingFactory(EntityNames.COMPANIES);
export const selectCompaniesItemSubmitting=selectEntityItemSubmittingFactory(EntityNames.COMPANIES);