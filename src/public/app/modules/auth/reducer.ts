import * as Immutable from 'immutable';
import {Action} from 'redux';

import * as Types from "./types"
import {Auth0Service} from "../../lib/services";


export interface IAuthState{
    isAuth:boolean;
    isRefreshing:boolean;
}

export class AuthState extends Immutable.Record({isAuth:Auth0Service.isLoggedIn(), isRefreshing:false}) implements IAuthState{
    isAuth:boolean;
    isRefreshing:boolean;
    constructor(){
        super({isAuth:Auth0Service.isLoggedIn(),isLoading:false,isRefreshing:false});
    }

}
export default function auth(state:AuthState=new AuthState(),action:Action){

    switch (action.type){
        case Types.AUTH_LOGGED_IN:
        case Types.AUTH_REFRESH_TOKEN_FAILURE:
        case Types.AUTH_REFRESH_TOKEN_SUCCESS:
            return new AuthState();
        case Types.AUTH_REFRESH_TOKEN_REQUEST:
            return state.set('isRefreshing',true);
        default:
            return state;
    }
};