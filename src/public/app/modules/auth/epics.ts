import * as Types from "./types";
import AuthActionCreators from "./creators";
import {of as of$} from "rxjs/Observable/of";

import { combineEpics } from 'redux-observable';
import {Types as AppType} from "../app";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/operator/mergeMap";
import "rxjs/add/operator/merge";
import "rxjs/add/operator/delay";
import "rxjs/add/operator/startWith";

import Auth0Service from "../../lib/services/Auth0Service";



const refreshTokenTimerEpic=(action$,state)=>
    action$
        .ofType(AppType.INIT,Types.AUTH_LOGGED_IN,Types.AUTH_REFRESH_TOKEN_SUCCESS)
        .filter(()=>state.getState().auth.isAuth)
        .delay(200000)
        .filter(()=>!state.getState().auth.isRefreshing)
        .mergeMap(()=>
                Auth0Service
                    .refresh()
                .map(AuthActionCreators.refreshTokenSuccess)
                .catch(err =>{
                    return of$(AuthActionCreators.refreshTokenFailure())
                })
                .startWith(AuthActionCreators.refreshTokenRequest())
            );

export default combineEpics(refreshTokenTimerEpic);