import * as Immutable from 'immutable';
import ILoginModel from "../../lib/interfaces/LoginModel";

export class LoginModel extends Immutable.Record({username:null,password:null}) implements ILoginModel{
    username:string;
    password:string;
}
