import Auth0Service from "../../lib/services/Auth0Service";

import {LoginModel} from "./models";

export function login(data:LoginModel){
        return Auth0Service.login(data)
        .toPromise()
}