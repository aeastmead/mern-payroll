import * as Types from "./types";
import * as Immutable from "immutable"

namespace AuthActionCreators {
    export const refreshTokenRequest = (res?: Object) => ({type: Types.AUTH_REFRESH_TOKEN_REQUEST, payload: {}});
    export const refreshTokenSuccess = (res?: Object) => ({type: Types.AUTH_REFRESH_TOKEN_SUCCESS, payload: {}});
    export const refreshTokenFailure = (res?: Object) => ({type: Types.AUTH_REFRESH_TOKEN_FAILURE, payload: {}});
    export function loggedIn(){
        return {
            type:Types.AUTH_LOGGED_IN
        }
    }

}
export default AuthActionCreators;