
import {EntityActionTypes } from "../../lib";
import ItemState,{EntityItemState} from "./models"

export default function item(state:ItemState=new ItemState(),{type,entity,payload}) {
    if(entity && !state.has(entity))
        return state;
    switch (type){
        case EntityActionTypes.FETCH_ITEM.SUCCESS:
            return state.setIn([entity,'id'],payload.result).setIn([entity,'isLoading'],false);
        case EntityActionTypes.FETCH_ITEM.REQUEST:
            return state.setIn([entity,'isLoading'],true);
        case EntityActionTypes.FETCH_ITEM.FAILURE:
            return state.setIn([entity,'isLoading'],false);
        case EntityActionTypes.SUBMIT.REQUEST:
            return state.setIn([entity,'submitting'],true);
        case EntityActionTypes.SUBMIT.SUCCESS:
            return state.setIn([entity,'submitting'],false).setIn([entity,'submitted'],true);
        case EntityActionTypes.SUBMIT.FAILURE:
            return state.setIn([entity,'submitting'],false);
        case EntityActionTypes.ITEM_RESET:
            return state.set(entity,new EntityItemState());
        default:
            return state;

    }

}