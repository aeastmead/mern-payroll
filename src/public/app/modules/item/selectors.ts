import * as Immutable from 'immutable';
import { INormalize,NormalizrService } from "../../lib";
import {RootState} from "../index";
export function selectEntityItemFactory<E>(entity:string){
    return (state:RootState)=>{
        let entityId=state.item.get(entity).id;
        if(!entityId)
        return undefined;
        return NormalizrService.denormalize<E>(entity,entityId,state.toJS());
    }
}
export function selectEntityItemLoadingFactory(entity:string):(state:RootState)=>boolean{
    return (state:RootState)=>state.item.get(entity).isLoading;
}
export function selectEntityItemSubmittingFactory(entity:string):(state:RootState)=>boolean{
    return (state:RootState)=>state.item.get(entity).submitting;
}