import * as Immutable from 'immutable';

export interface IEntityItemState{
    id:string;
    isLoading:boolean;
}
export class EntityItemState extends Immutable.Record({
    id:null,
    isLoading:false
}) implements IEntityItemState{
    id:string;
    isLoading:boolean;
}
export default class ItemState extends Immutable.Record({
    campaigns:new EntityItemState(),
    companies:new EntityItemState(),
    employees:new EntityItemState()}){
    campaigns:EntityItemState;
    companies:EntityItemState;
    employees:EntityItemState;
}