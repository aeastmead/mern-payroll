import * as Immutable from "immutable";
import {FormState as IFormState} from "redux-form";

export type FormState=Immutable.Map<string,IFormState>;
export const FormState=Immutable.Map<string,IFormState>();

export {reducer as default} from "redux-form";
