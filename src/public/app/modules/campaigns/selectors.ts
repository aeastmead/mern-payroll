import {selectEntityPageFactory,selectEntityPageLoadingFactory} from "../paging";
import {Campaign} from "./models";
import {selectEntityItemFactory, selectEntityItemLoadingFactory, selectEntityItemSubmittingFactory} from "../item";
import EntityNames from "../entityNames";

export const selectCampaignsPage=selectEntityPageFactory<Campaign>(EntityNames.CAMPAIGNS);
export const selectCampaignsPageLoading=selectEntityPageLoadingFactory(EntityNames.CAMPAIGNS);
export const selectCampaignsItem=selectEntityItemFactory<Campaign>(EntityNames.CAMPAIGNS);
export const selectCampaignsItemLoading=selectEntityItemLoadingFactory(EntityNames.CAMPAIGNS);
export const selectCampaignsItemSubmitting=selectEntityItemSubmittingFactory(EntityNames.CAMPAIGNS);