import { EntityReducerFactory,EntityActionCreatorFactory, EntityEpicsFactory} from "../../lib";

import EntityNames from "../entityNames";

export default EntityEpicsFactory(EntityNames.CAMPAIGNS);