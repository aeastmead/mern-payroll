import {EntityActionsFactory} from "../../lib";
import EntityNames from "../entityNames";

export default EntityActionsFactory(EntityNames.CAMPAIGNS);