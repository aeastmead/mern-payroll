import { EntityReducerFactory} from "../../lib";
import EntityNames from "../entityNames";
import {Campaign} from "./models";

export default EntityReducerFactory<Campaign>(EntityNames.CAMPAIGNS,Campaign);
