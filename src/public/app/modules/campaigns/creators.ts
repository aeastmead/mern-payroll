import {EntityActionCreatorFactory} from "../../lib";
import EntityNames from "../entityNames";

export default EntityActionCreatorFactory(EntityNames.CAMPAIGNS);