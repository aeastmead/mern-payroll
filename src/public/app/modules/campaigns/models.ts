import {EntityFactory,EntityStateFactory,EntityState, ActiveDates, IEntity} from "../../lib";

interface IAttributes{
    name:string;
    activeDates:ActiveDates;
}
export interface ICampaign extends IEntity{
    name:string;
    activeDates:ActiveDates;
}
export class Campaign extends EntityFactory<IAttributes>({name:null, activeDates:new ActiveDates()}){
    name:string;
    activeDates:ActiveDates;

}
export type CampaignsState=EntityState<Campaign>;
export const CampaignsState:CampaignsState = EntityStateFactory<Campaign>();