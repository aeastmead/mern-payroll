import Types from "./types";
import {ListActionCreators} from "./creators";

import {of as of$} from "rxjs/Observable/of";

import { combineEpics } from 'redux-observable';
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import "rxjs/add/operator/mergeMap";
import "rxjs/add/operator/merge";
import "rxjs/add/operator/delay";

import APIService from "../../lib/services/APIService";

const fetchListRequestEpic=(action$)=>
    action$.ofType(Types.FETCH_LIST_REQUEST)
    .map(action=>action.entity)
        .mergeMap(entity=>APIService.getAll(entity)
            .map(res=>ListActionCreators.fetchListSuccess(entity,res.data))
            .catch(err=>of$(ListActionCreators.fetchListFailure(entity)))
        );

export default combineEpics(fetchListRequestEpic);