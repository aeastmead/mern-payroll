import * as Immutable from 'immutable';
import {Company} from "../companies";
import {Campaign} from "../campaigns"

export interface IEntityListState<E>{
    items:Immutable.List<E>;
    isLoading:boolean;
}
export class EntityListState<E> extends Immutable.Record({
    isLoading:false,
    items:Immutable.List<E>()
}) implements IEntityListState<E>{
    items:Immutable.List<E>;
    isLoading:boolean;
}

export default class ListsState extends Immutable.Record({
    companies:new EntityListState<Company>(),
    campaigns:new EntityListState<Campaign>()
}) {
   companies:EntityListState<Company>;
   campaigns:EntityListState<Campaign>;
}
