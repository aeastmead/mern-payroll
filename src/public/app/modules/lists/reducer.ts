import * as Immutable from 'immutable';
import * as _ from "lodash";
import Types from "./types";
import ListsState,{EntityListState} from "./models"
import {RootState} from "../index";

function entitiesToMap(obj:Object){
    
}
export default function paging(state:ListsState=new ListsState(),{type,entity,payload}) {
    if(entity && !state.has(entity))
        return state;
    switch (type){
        case Types.FETCH_LIST_SUCCESS:
            return state.setIn([entity,'items'],Immutable.List.of(Immutable.fromJS(payload.items)))
            .setIn([entity,'isLoading'],true);
        case Types.FETCH_LIST_REQUEST:
            return state.setIn([entity,'isLoading'],true);
        case Types.FETCH_LIST_FAILURE:
            return state.setIn([entity,'isLoading'],false);
        case Types.CLEAR_LIST:
            return state.set(entity,new EntityListState<any>());
        default:
            return state;

    }

}