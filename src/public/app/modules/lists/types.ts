
namespace ListActionTypes{
    export const FETCH_LIST_REQUEST="entities/FETCH_LIST_REQUEST";
    export const FETCH_LIST_SUCCESS="entities/FETCH_LIST_SUCCESS";
    export const FETCH_LIST_FAILURE="entities/FETCH_LIST_SUCCESS";
    export const CLEAR_LIST="entities/CLEAR_LIST";
}
export default ListActionTypes;