import { INormalize,NormalizrService } from "../../lib";
import {RootState} from "../index";

import EntityNames from "../entityNames";
import ListsState,{EntityListState} from "./models"
import {Company} from "../companies";
import {Campaign} from "../campaigns"

export function selectEntityListFactory<E>(entity:string){
    return function(state:RootState):E[]{
        return (state.lists.get(entity) as EntityListState<E>).items.toArray();
    }
}
export function selectEntityListLoadingFactory(entity:string){
    return function(state:RootState):boolean{
      return  state.lists.get(entity).isLoading;
    }
}
export const selectCompaniesList=selectEntityListFactory<Company>(EntityNames.COMPANIES);
export const selectCompaniesListLoading=selectEntityListLoadingFactory(EntityNames.COMPANIES);

export const selectCampaignsList=selectEntityListFactory<Campaign>(EntityNames.CAMPAIGNS);

export const selectCampaignsListLoading=selectEntityListLoadingFactory(EntityNames.CAMPAIGNS);