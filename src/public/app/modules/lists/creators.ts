import Types from "./types";

import EntityNames from "../entityNames";

export namespace ListActionCreators{
    export function fetchListRequest(entity:string){
        return {
            type:Types.FETCH_LIST_REQUEST,
            entity:entity
        }
    }
    export function fetchListSuccess(entity:string, items:any[]){
        return {
            type:Types.FETCH_LIST_SUCCESS,
            entity,
            payload:{items}
        }
    }
    export function fetchListFailure(entity:string){
        return {
            type:Types.FETCH_LIST_FAILURE,
            entity:entity
        }
    }
    export function clearList(entity:string){
        return {
            type:Types.CLEAR_LIST,
            entity:entity
        }
    }
}
function listActionCreatorsFactory(entity:string){
    return {
        fetchListRequest:()=>ListActionCreators.fetchListRequest(entity),
        fetchListSuccess:(items:any[])=>ListActionCreators.fetchListSuccess(entity,items),
        fetchListFailure:()=>ListActionCreators.fetchListFailure(entity)
    }
}
export const clearCompaniesList=()=>ListActionCreators.clearList(EntityNames.COMPANIES);
export const getCompaniesList=()=>ListActionCreators.fetchListRequest(EntityNames.COMPANIES);
export const clearCampaignsList=()=>ListActionCreators.clearList(EntityNames.CAMPAIGNS);
export const getCampaignsList=()=>ListActionCreators.fetchListRequest(EntityNames.CAMPAIGNS);