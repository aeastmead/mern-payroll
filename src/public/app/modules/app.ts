
export const Types={
    INIT:'app/INIT'
};

export const AppActions={
    init:()=>({
        type:Types.INIT,
        payload:{}
    })
}