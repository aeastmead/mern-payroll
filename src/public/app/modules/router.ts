import {LOCATION_CHANGE} from "react-router-redux";
import * as Immutable from "immutable";
import {Location} from "history"

export class RouterState extends Immutable.Record({location:null}){
    location:Location|null;
}

export default function router(state:RouterState=new RouterState(),{type,payload}:{type:string,payload?:Location}={type:null}) {
    if(type===LOCATION_CHANGE){
        return state.set('location',payload)
    }
    return state;
}