import {combineReducers} from "redux-immutable";
import {combineEpics} from "redux-observable";
import {Reducer} from "redux"
import * as Immutable from "immutable";
import router,{RouterState} from "./router";
import form,{FormState} from "./form"
import auth,{AuthState,epics as AuthEpics} from './auth';
import campaigns,{CampaignsState,CampaignsEpics} from './campaigns';
import companies,{CompaniesState,CompaniesEpics} from './companies';
import employees,{EmployeesState,EmployeesEpics} from './employees';
import lists,{ListsState,ListsEpics} from './lists';
import paging,{PagingState} from "./paging"
import item,{ItemState} from "./item"


export const rootEpic=combineEpics(AuthEpics,CampaignsEpics,CompaniesEpics,EmployeesEpics,ListsEpics );
 const rootReducer:Reducer<RootState>=combineReducers<RootState>({auth,form,router,campaigns,companies,employees,  paging, item,lists});
export default rootReducer;
export class RootState extends Immutable.Record({
    auth:new AuthState(),
    form:FormState,
    router:new RouterState(),
    campaigns:CampaignsState,
    companies:CompaniesState,
    employees:EmployeesState,
    lists:new ListsState(),
    paging:new PagingState(),
    item:new ItemState()
}){
    auth:AuthState;
    form:FormState;
    router:RouterState;
    campaigns:CampaignsState;
    companies:CompaniesState;
    employees:EmployeesState;
    lists:ListsState;
    paging:PagingState;
    item:ItemState;

}