import {selectEntityPageFactory,selectEntityPageLoadingFactory} from "../paging";
import {Employee} from "./models";
import EntityNames from "../entityNames";
import {selectEntityItemFactory, selectEntityItemLoadingFactory, selectEntityItemSubmittingFactory} from "../item";

export const selectEmployeesPage=selectEntityPageFactory<Employee>(EntityNames.EMPLOYEES);
export const selectEmployeesPageLoading=selectEntityPageLoadingFactory(EntityNames.EMPLOYEES);
export const selectEmployeesItem=selectEntityItemFactory<Employee>(EntityNames.EMPLOYEES);
export const selectEmployeesItemLoading=selectEntityItemLoadingFactory(EntityNames.EMPLOYEES);
export const selectEmployeesItemSubmitting=selectEntityItemSubmittingFactory(EntityNames.EMPLOYEES);