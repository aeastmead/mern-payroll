import {EntityFactory,EntityStateFactory,EntityState,Contact, Name, ActiveDates, IEntity} from "../../lib";
import {ICompany} from "../companies"

interface IAttributes{
    name:Name;
    company:string;
    contact:Contact;
    activeDates:ActiveDates;

}
export interface IEmployee extends IEntity{
    name:Name;
    company:ICompany;
    contact:Contact;
    activeDates:ActiveDates;

}
export class Employee extends EntityFactory<IAttributes>({name:new Name(), contact:new Contact(), activeDates:new ActiveDates(), company:null}){
    name:string;
    owner:Name;
    contact:Contact;
    company:string;
    activeDates:ActiveDates;

}
export type EmployeesState=EntityState<Employee>;
export const EmployeesState:EmployeesState = EntityStateFactory<Employee>();