import { EntityEpicsFactory} from "../../lib";
import EntityNames from "../entityNames";

export default EntityEpicsFactory(EntityNames.EMPLOYEES);