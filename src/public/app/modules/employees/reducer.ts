import { EntityReducerFactory} from "../../lib";
import {Employee} from "./models";
import EntityNames from "../entityNames";

export default EntityReducerFactory<Employee>(EntityNames.EMPLOYEES,Employee);
