import * as Immutable from 'immutable';
import * as _ from 'lodash'
import { INormalize,NormalizrService } from "../../lib";

export interface IEntityPaginationState{
    total:number;
    limit?:number;
    offset:number;
    include?:string[];
    sorting?:string;
    ids:string[];
    isLoading:boolean;
    fromNormalizrPage(page:INormalize.INormalizedPagination):Immutable.Map<string,any>;
}
export class EntityPaginationState extends Immutable.Record({total:null,
    limit:null,
    offset:0,
    include:[],
    sorting:null,
    ids:[],
    isLoading:false
}) implements IEntityPaginationState{
    total:number;
    limit:number;
    offset:number;
    include:string[];
    sorting:string;
    ids:string[];
    isLoading:boolean;
    fromNormalizrPage(page:INormalize.INormalizedPagination){
        return this.merge({total:page.total,limit:page.limit,offset:page.offset,include:page.include,sorting:page.sort})
            .set('ids',page.result);
    }
}

export default class PagingState extends Immutable.Record({
    campaigns:new EntityPaginationState(),
    employees:new EntityPaginationState(),
companies:new EntityPaginationState()
    }){
    campaigns:EntityPaginationState;
    employees:EntityPaginationState;
    companies:EntityPaginationState;
}