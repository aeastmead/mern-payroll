import { EntityActionTypes } from "../../lib";
import PagingState from "./models"
import {RootState} from "../index";

export default function paging(state:PagingState=new PagingState(),{type,entity,payload}) {
    if(entity && !state.has(entity))
        return state;
    switch (type){
        case EntityActionTypes.FETCH_PAGE.SUCCESS:

            return state.set(entity,state.get(entity).fromNormalizrPage(payload).set('isLoading',false));
        case EntityActionTypes.FETCH_PAGE.REQUEST:
            return state.setIn([entity,'isLoading'],true);
        case EntityActionTypes.FETCH_PAGE.FAILURE:
            return state.setIn([entity,'isLoading'],false);
        default:
            return state;

    }

}