import { INormalize,NormalizrService } from "../../lib";
import {RootState} from "../index";

export function selectEntityPageFactory<E>(entity:string){
    return function(state:RootState):E[]{
        return NormalizrService.denormalize<E[]>(entity,state.paging.get(entity).ids,state.toJS());
    }
}
export function selectEntityPageLoadingFactory(entity:string):(state:RootState)=>boolean{
    return (state:RootState)=>state.paging.get(entity).isLoading;
}