import "./styles/main";
import "./vendor";

import * as React from "react";
import * as ReactDom from 'react-dom';
import {createBrowserHistory, History} from 'history';


import configureStore from './store';

import App from "./Features";

const history:History = createBrowserHistory();
const store=configureStore(history);

ReactDom.render(
    <App store={store} history={history}/>,
    document.getElementById('root')
);