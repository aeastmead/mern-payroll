import { createStore, Store, applyMiddleware, compose  } from 'redux';
import {  routerMiddleware} from 'react-router-redux';


import { createEpicMiddleware } from 'redux-observable';
import {createLogger} from 'redux-logger';
import {History} from 'history';
import rootReducer,{ rootEpic,RootState} from '../modules';
import {AppActions} from "../modules/app"

export default function configureStore(history:History): Store<RootState> {
    const logger = createLogger({
        stateTransformer: (state) => state.toJS()
    });
    const routeMiddleware=routerMiddleware(history);
    const epicMiddleware=createEpicMiddleware<any, RootState>(rootEpic);
    const store = createStore<RootState>(rootReducer, new RootState(), compose(applyMiddleware(logger,routeMiddleware,epicMiddleware)));
    store.dispatch(AppActions.init());
    return store;
}
