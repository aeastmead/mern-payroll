import * as bodyParser from 'body-parser';
import {SetupJwt, LogMiddleware} from "./lib";
import * as express from 'express';


export default function SetupMiddlewares(app:express.Application){
    SetupJwt();
   return app.use(bodyParser.urlencoded({
            extended: true
        }))
        .use(LogMiddleware())
}