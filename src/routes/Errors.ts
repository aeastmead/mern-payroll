import * as E from 'express';
import * as serveStatic from "serve-static";
import {ErrorLogMiddleware} from "../lib";
import * as path from "path";

const clientDir=path.join(__dirname,'../public');

export default ()=>
    E.Router().use(ErrorLogMiddleware).use(({status=400,...msg}:any,req:E.Request,res:E.Response,next:E.NextFunction)=>{
        res.status(status).json(msg||{});
    })