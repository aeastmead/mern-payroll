export {default as APIRoutes} from "./api";
export {default as ErrorRoutes} from "./Errors";
export {default as StaticRoutes} from "./Statics"; 