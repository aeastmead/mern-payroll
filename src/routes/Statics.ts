import * as E from 'express';
import * as serveStatic from "serve-static";
import * as path from "path";

const clientDir=path.join(__dirname,'../public');

export default ()=>
    E.Router().use(serveStatic(clientDir,{extensions:['js','css','json','html']}))
    .get("*",(req:E.Request,res:E.Response)=>{res.sendFile(`${clientDir}/index.html`)})