import * as E from 'express';
import * as mongoose from "mongoose";
import * as _ from "lodash";
import {Observable} from "rxjs/Observable";

import {BadRequest,NotFound} from 'http-errors';
import {requestParser,populateParser} from "./helpers";

declare module 'express'{
    interface PGRequest<T extends mongoose.RxDocument> extends E.Request{
        pgOptions?:mongoose.PaginateOptions;
        pgQuery?:Object;
        params:{id:string} & any;
        doc:T;
    }
}

export interface EntityMiddleware<T extends mongoose.Document>{
    qsParser:E.RequestHandler,
    findById:E.RequestHandler;
}
export interface EntityMiddlewareFactory<T extends mongoose.RxDocument>{
    (repository:mongoose.RxModel<T>):EntityMiddleware<T>;
}

export default function EntityMiddlewareFactory<T extends mongoose.RxDocument>({findById$}:mongoose.RxModel<T>):EntityMiddleware<T>{
    return{
        qsParser:(req:E.PGRequest<T>,res:E.Response,next:E.NextFunction)=>{
            let {options,query}=requestParser(req.query);
            req.pgOptions=options;
            req.pgQuery=query;
            next();
        },
        findById:(req:E.PGRequest<T>,res:E.Response,next:E.NextFunction)=>{
            if(_.isNil(req.params.id)){
                next(new BadRequest());
                return;
            }
            let populate=req.pgOptions && req.pgOptions.populate ? req.pgOptions.populate:undefined;
            findById$(req.params.id,populate as string[])
            .subscribe(doc=>{
                req.doc=doc;
                next();
            },
            err=>{
                next(new NotFound());
            })
        }
    } 
}