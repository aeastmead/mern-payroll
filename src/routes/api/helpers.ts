import * as mongoose from "mongoose";
import * as mongoosePaginate from 'mongoose-paginate';
import * as pluralize from 'pluralize';
import * as _ from "lodash";
import {of as of$} from "rxjs/Observable/of";
import {_throw as throw$} from "rxjs/Observable/throw";
import {bindNodeCallback as bindNodeCallback$} from "rxjs/Observable/bindNodeCallback"
import {Observable} from "rxjs/Observable";
import {Observer} from "rxjs/Observer";

import * as createError from 'http-errors';

interface IRequestFilter{
    limit?:number;
    offset?:number;
    sort_field?:string;
    sort_direction?:string;
    include?:string[]|string;
    [fields:string]:any;
}
interface IMongooseError{
    errors:{[path:string]:mongoose.ValidationError};
}
export function isEmpty(val:any){
    if(_.isUndefined(val))
    return true;
    if(_.isNil(val))
    return true;
    if(_.isObject(val)){
        return Object.keys(val).length>0;
    }
    if(_.isString(val))
        return val.trim().length<=0;
    return _.isEmpty(val);
}
export function camelCaser(snakeObj:Object):Object{
        if(_.isUndefined(snakeObj))
            return {};
        if(!_.isObject(snakeObj))
        return {};
        let camelCased:Object={};
        Object.keys(snakeObj).forEach(key=>camelCased[_.camelCase(key)]=snakeObj[key]);
        return camelCased
}
export const populateParser=(pop:string|string[])=>{
        if(!isEmpty(pop) && (_.isString(pop)||_.isArray(pop))){
            if(_.isString(pop))
                pop=[pop as string];
            return pop.map(name=>_.camelCase(name));
        }
        return undefined;
}
export function requestParser({limit,offset,sort_field,sort_direction,include,...rawQuery}:IRequestFilter, noLimit:boolean=false):{options:mongoose.PaginateOptions,query:Object}{
        if(noLimit){
            limit=undefined
        }
        else if(!limit || (limit && (limit<=0||limit>25)))
            limit=25;
        if(!offset || (offset&&offset<0))
            offset=0;
        if(!sort_field)
            sort_field='createdAt';
        sort_field=_.camelCase(sort_field);
        if('asc'!=sort_direction)
            sort_direction='desc';
        return {options:{
            populate:populateParser(include),
            limit, offset,
            sort:{[sort_field]:sort_direction}
        }, query:camelCaser(rawQuery)}
}
