import * as E from 'express';
import * as _ from "lodash";
import createEntityRouter from "./createEntityRouter";
import {CampaignModel,Campaign,EmployeeModel,Employee,CompanyModel,Company} from "../../models";

export default ()=>
    E.Router()
    .use("/campaigns",createEntityRouter<Campaign>(CampaignModel))
    .use("/companies",createEntityRouter<Company>(CompanyModel))
    .use("/employees",createEntityRouter<Employee>(EmployeeModel));