import * as E from 'express';
import * as mongoose from "mongoose";
import EntityMiddlewareFactory from "./EntityMiddlewareFactory";
import EntityFnFactory  from "./EntityFnFactory";

export default function createEntityRouter<T extends mongoose.RxDocument>(model:mongoose.RxModel<T>){
    const {qsParser,findById}=EntityMiddlewareFactory<T>(model);
    const {search,detail,update,create, all}= EntityFnFactory<T>(model);

    return E.Router().get("",qsParser,search)
    .get("/all",qsParser,all)
    .get("/:id([A-Za-z0-9_]{5,})",qsParser,qsParser,findById,detail)
    .post("",create)
    .put("/:id",qsParser,findById,update);

}