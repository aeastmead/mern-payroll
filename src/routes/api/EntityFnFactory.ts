import * as E from 'express';
import * as mongoose from "mongoose";
import {Observable} from "rxjs/Observable";
import "rxjs/add/operator/map";
import {BadRequest} from 'http-errors';

interface IEntityRoute<T extends mongoose.Document>{
    model:mongoose.PaginateModel<T>;
    name:string;
}
interface ISingleResult<T>{
    data:T;
    kind:string;
}
interface IPaginationResult<T>{
    total:number;
    limit?:number;
    offset?:number;
    include?:string[];
    sort_direction:string;
    sort_field:string;
    data:T[];
    kind:string;
}
export interface EntityFn<T extends mongoose.RxDocument>{
    search:E.RequestHandler;
    all:E.RequestHandler;
    detail:E.RequestHandler;
    update:E.RequestHandler;
    create:E.RequestHandler
}
export interface EntityFnFactory<T extends mongoose.RxDocument>{
    (model:mongoose.RxModel<T>):EntityFn<T>;
}

export default function EntityFnFactory<T extends mongoose.RxDocument>({all$,find$, insert$,getKind}:mongoose.RxModel<T>){
    let kind:string=getKind();

    function responseHandler(observable$:Observable<mongoose.RxPaginationResult<T>|T>,{status}:E.Response,next:E.NextFunction){
           observable$
           .map((result:mongoose.RxDocument|mongoose.RxPaginationResult<T>)=>Object.assign({},result,{kind}))
            .subscribe(pgResponse=>status(200).json(pgResponse),
            err=>next(err.status? err:new BadRequest(err.message)));
    }
    return {
        search:({pgQuery={},pgOptions={}}:E.PGRequest<T>,res:E.Response,next:E.NextFunction)=>{
            responseHandler(find$(pgQuery,pgOptions),res,next)
        },
        all:({pgOptions={}}:E.PGRequest<T>,res:E.Response,next:E.NextFunction)=>{
            responseHandler(all$(pgOptions.sort,pgOptions.populate as string[]),res,next);
        },
        detail:({doc:data}:E.PGRequest<T>,res:E.Response,next:E.NextFunction)=>{
            res.status(200).json({kind,data})
        },
        update:({doc,body}:E.PGRequest<T>,res:E.Response,next:E.NextFunction)=>{
            responseHandler(doc.update$(body),res,next);
        },
        create:({body}:E.PGRequest<T>,res:E.Response,next:E.NextFunction)=>{
            responseHandler(insert$(body),res,next);
        }
    }
}