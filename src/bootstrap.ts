/// <reference path="typings.d.ts"/>
import * as bodyParser from 'body-parser';
import * as express from 'express';
import SetupMiddlewares from "./middlewares";
import {APIRoutes,StaticRoutes,ErrorRoutes} from "./routes"
import {Connect$,Authenticate} from "./lib"

let app=express();
const port=3000;
SetupMiddlewares(app);

app.use("/api/v1",Authenticate(),APIRoutes());
app.use(StaticRoutes());

app.use(ErrorRoutes());

Connect$()
.subscribe(conn=>{
    app.listen(port)
});
