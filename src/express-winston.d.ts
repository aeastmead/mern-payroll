
declare module "express-winston"{
    import * as winston from "winston";
import {RequestHandler, ErrorRequestHandler} from "express";
    export function errorLogger(options:Object):ErrorRequestHandler
    export function logger(options:Object):RequestHandler
}