const gulp=require('gulp'),
ts= require('gulp-typescript'),
path=require('path'),
del = require('del'),
    gls = require('gulp-live-server'),
webpack=require('webpack'),
gutil = require('gulp-util'),
browserSync = require('browser-sync');
const OUT_PATH="./dist";
const SRC_PATH="./src";
const SRC_PUBLIC_PATH=`${SRC_PATH}/public`;
const appPaths={
    src:"./src",
    client:`${this.src}/public`,
    dist:"./dist"
}

gulp.task('clean',()=>del([OUT_PATH]));

gulp.task('build:server',function() {

    let tsProject = ts.createProject(`./tsconfig.json`);
    return gulp.src([`${SRC_PATH}/**/*.ts`,`!${SRC_PUBLIC_PATH}/**/*`])
        .pipe(tsProject())
        .js.pipe(gulp.dest(OUT_PATH));
});
gulp.task('copy:json',function() {
    return gulp.src(`${SRC_PATH}/**/*.json`)
        .pipe(gulp.dest(`${OUT_PATH}`))
});
gulp.task('build:client',(cb)=>{
    "use strict";
    const webpackConfig=require('./webpack.config');

    webpack(webpackConfig, (err, stats) => {
        if(err) throw new gutil.PluginError("webpack:build", err);
        console.log(stats.toString({
            warnings:false,
    chunks: true,  // Makes the build much quieter
    colors: true    // Shows colors in the console
  }));
        cb();
    });
});




let hasInit=false;
gulp.task('bs',(done)=>{
    "use strict";
    if(!hasInit) {
        hasInit=true;
        browserSync.init({
            proxy: "http://localhost:3000",
            browser: ['firefox'],
            port: 4000,
            socket: {
    domain: 'localhost:4000'
}
        }, done);
    }
    else {
        setTimeout(()=>{
            browserSync.reload();
            done();
        },2000)
    }
});
let server;
gulp.task('serve:start',function (cb) {
    if(!server)
        server=gls('./dist/bootstrap.js',{env:{NODE_ENV:'local'}},false);
     server.start();
     setTimeout(()=>{
         "use strict";
         cb();
     },2000)


});
gulp.task('serve:stop',function () {
    return server.stop();

});
gulp.task('watch',function () {
    gulp.watch(['src/**/*','./webpack.config.js'],gulp.series('serve:stop','build:client', 'build:server','copy:json','serve:start','bs'))
});
gulp.task('build',gulp.series('clean','build:server','build:client','copy:json'));
gulp.task('serve',gulp.series('build','serve:start','bs','watch'));
gulp.task('default',gulp.series('build'));
