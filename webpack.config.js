const path=require('path');
const { CheckerPlugin } = require('awesome-typescript-loader');
const HtmlWebpackPlugin=require('html-webpack-plugin');
const rootDir=path.join(__dirname,'./src/public');
const outPath=path.join(__dirname,'./dist/public');
const scssPath=path.join(rootDir,'./scss');
const assetPath=path.join(rootDir,'./assets');
module.exports = {
    context:rootDir,
  entry: {
    main:"./app/index.tsx"
  },
  output: {
    filename: "[name].bundle.js",
    path: outPath,
    publicPath:'/'
  },
  resolve: {
    extensions: ['.js', '.ts', '.tsx','.css','.scss']
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
          use: [
          {
            loader: "awesome-typescript-loader",
            options:{
              configFileName:"./tsconfig.json"
            }
          }
        ],
        exclude: [/node_modules/]
      },
        {
        test: /\.(ico|eot|otf|webp|ttf|woff|woff2)(\?.*)?$/,
        use: 'file-loader?limit=100000'
      },
      {
        test: /\.(jpe?g|png|gif|svg)$/i,
        use: [
          'file-loader?limit=100000',
          {
            loader: 'img-loader',
            options: {
              enabled: true,
              optipng: true
            }
          }]
      },
      {
    test: /\.css$/,
    loaders: ['style-loader', 'css-loader', 'resolve-url-loader']
  },
  {
    test: /\.scss$/,
            use: [{
                loader: "style-loader"
            }, {
                loader: "css-loader"
            }, {
                loader: "sass-loader"
            }]
  }
    ]
  },
  plugins: [

    new CheckerPlugin(),
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template:'index.html'
    })
  ]
};
